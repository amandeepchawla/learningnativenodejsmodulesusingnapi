#include "functionExample.h"

namespace funcEx {
    std::string sayHello() {
        return "Hello World!";
    }

    int64_t add(int64_t a, int64_t b) {
        return a + b;
    }

    Napi::String SayHelloWrapped(const Napi::CallbackInfo & info) {
        Napi::Env env = info.Env();
        Napi::String returnValue = Napi::String::New(env, sayHello());
        return returnValue;
    }

    Napi::Number AddWrapper(const Napi::CallbackInfo & info) {
        Napi::Env env = info.Env();
        if (info.Length() < 2 || !info[0].IsNumber() || !info[1].IsNumber()) {
            Napi::TypeError::New(env, "Number expected").ThrowAsJavaScriptException();
        }

        Napi::Number a = info[0].As<Napi::Number>();
        Napi::Number b = info[1].As<Napi::Number>();
        int rV = add(a.Int64Value(), b.Int64Value());
        return Napi::Number::New(env, rV);
    }

    Napi::Object Init(Napi::Env env, Napi::Object exports) {
        exports.Set("hello", Napi::Function::New(env, SayHelloWrapped));
        exports.Set("add", Napi::Function::New(env, AddWrapper));
        return exports;
    }
}