#include <napi.h>

namespace funcEx {
    Napi::String SayHelloWrapped(const Napi::CallbackInfo & info);
    Napi::Number AddWrapper(const Napi::CallbackInfo & info);
    Napi::Object Init(Napi::Env env, Napi::Object exports);
}